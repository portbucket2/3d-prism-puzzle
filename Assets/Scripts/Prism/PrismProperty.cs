﻿using UnityEngine;

public class PrismProperty : MonoBehaviour
{

    #region OnUnityEditor

    private void OnEnable()
    {
        CalculateRefractionOfLight(angleOfIncident);
    }

    private void OnDrawGizmosSelected()
    {
        CalculateRefractionOfLight(angleOfIncident);
    }

    #endregion

    #region Custom Variables



    [System.Serializable]
    public struct RefractedLightProperty
    {
        public UnitVector       reftractionDirection;
        public Color            refractionColor;
        public LineRenderer     refractionLine;
        public ParticleSystem   refractionImpact;
    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR

    [Space(5.0f)]
    [Header("Property   :   Editor")]
    public Vector3 angleOfIncident;

#endif

    [Space(10.0f)]
    [Range(1f,1000f)]
    public float                    maxTravelDistanceOfLight = 100.0f;
    public RefractedLightProperty[] refractedLightProperties;

    #endregion

    #region Private Variables

    private int m_NumberOfRefractedLightProperty;
    private Transform m_TransformReference;
    private Vector3 m_AngleOfIncident;

    #endregion

    #region MonoBehaviour

    private void Start()
    {
        //Debug
        Initialization();
        CalculateRefractionOfLight(angleOfIncident);
    }
    #endregion

    #region Configuretion

    private void Initialization() {

        m_TransformReference                = transform;
        m_NumberOfRefractedLightProperty    = refractedLightProperties.Length;
    }

    #endregion

    #region Public Callback

    public void CalculateRefractionOfLight(Vector3 t_AngleOfIncident) {

        m_AngleOfIncident = t_AngleOfIncident;

        for (int i = 0; i < m_NumberOfRefractedLightProperty; i++) {

            UnitVector  t_RefractionDirection       = refractedLightProperties[i].reftractionDirection;
            Vector3 t_DirectionOfRefractedLight = Vector3.Normalize(new Vector3(
                    t_RefractionDirection.x,
                    t_RefractionDirection.y,
                    t_RefractionDirection.z
                ));

            Debug.DrawLine(
                    m_TransformReference.position,
                    t_DirectionOfRefractedLight * maxTravelDistanceOfLight,
                    refractedLightProperties[i].refractionColor,
                    0.0167f
                );
        }
    }

    #endregion
}
