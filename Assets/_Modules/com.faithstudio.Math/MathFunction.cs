﻿namespace com.faithstudio.Math
{
	using System;
	using UnityEngine;

    using GameplayService;

    public class MathFunction : MonoBehaviour
	{

		public static MathFunction Instance;

		void Awake()
		{

			if (Instance == null)
			{

				Instance = this;
				DontDestroyOnLoad(gameObject);
			}
			else if (Instance != this)
			{

				Destroy(gameObject);
			}

			PreprocessForTranslationByCamera();
		}

		public Vector2 ConvertUnitVectorToVector(UnitVector2D t_UnitVector)
		{

			Vector2 t_Result = new Vector2(
				t_UnitVector.x,
				t_UnitVector.y
			);

			return t_Result;
		}

		public Vector3 ConvertUnitVectorToVector(UnitVector t_UnitVector)
		{

			Vector3 t_Result = new Vector3(
				t_UnitVector.x,
				t_UnitVector.y,
				t_UnitVector.z
			);

			return t_Result;
		}

		public Vector3 GetUnitVector(Vector3 startingPosition, Vector3 targetPosition)
		{

			Vector3 m_UnitVector = Vector3.zero;

			Vector3 m_Difference = new Vector3(
				targetPosition.x - startingPosition.x,
				targetPosition.y - startingPosition.y,
				targetPosition.z - startingPosition.z
			);

			float m_TotalDistance = Mathf.Abs(m_Difference.x) + Mathf.Abs(m_Difference.y) + Mathf.Abs(m_Difference.z);

			if (m_TotalDistance == 0f)
			{

				m_UnitVector = Vector2.zero;
			}
			else
			{

				m_UnitVector = new Vector3(
					m_Difference.x / m_TotalDistance,
					m_Difference.y / m_TotalDistance,
					m_Difference.z / m_TotalDistance
				);
			}

			return m_UnitVector;
		}

		public float GetRotationInDegree(Vector2 t_DirectionVector)
		{

			float t_Result = 0.0f;

			if (t_DirectionVector.x > t_DirectionVector.y)
			{

				if (t_DirectionVector.y >= 0f)
				{

					t_Result = Vector2.Angle(t_DirectionVector, Vector2.right);
				}
				else
				{

					t_Result = 360.0f - Vector2.Angle(t_DirectionVector, Vector2.right);
				}
			}
			else
			{

				if (t_DirectionVector.y >= 0f)
				{

					t_Result = Vector2.Angle(t_DirectionVector, Vector2.right);
				}
				else
				{

					t_Result = 360.0f - Vector2.Angle(t_DirectionVector, Vector2.right);
				}
			}

			return t_Result;
		}

		public float GetRotationInRadian(Vector2 t_DirectionVector)
		{

			return ((GetRotationInDegree(t_DirectionVector) * Mathf.PI) / 180.0f);
		}

		public PriorityBound[] GetPriorityBound(float[] priorityList)
		{

			PriorityBound[] t_Result = new PriorityBound[priorityList.Length];

			float[] t_SortedPriorityList = new float[priorityList.Length];
			System.Array.Copy(priorityList, t_SortedPriorityList, t_SortedPriorityList.Length);

			float t_TotalPriority = 0.0f;
			for (int index = 0; index < t_SortedPriorityList.Length; index++)
				t_TotalPriority += t_SortedPriorityList[index];

			//Debug.Log("Total Priority : " + t_TotalPriority);

			System.Array.Sort(t_SortedPriorityList);

			bool[] t_IsFoundMatch = new bool[priorityList.Length];

			for (int pointerIndex = 0; pointerIndex < priorityList.Length; pointerIndex++)
			{

				for (int traverseIndex = 0; traverseIndex < t_SortedPriorityList.Length; traverseIndex++)
				{

					if (!t_IsFoundMatch[traverseIndex] &&
						priorityList[pointerIndex] == t_SortedPriorityList[traverseIndex])
					{

						t_IsFoundMatch[traverseIndex] = true;

						float t_LowerBound = 0.0f;
						for (int calculatedIndex = t_SortedPriorityList.Length - 1; calculatedIndex > traverseIndex; calculatedIndex--)
						{
							t_LowerBound += t_SortedPriorityList[calculatedIndex];
						}

						t_Result[pointerIndex].lowerPriority = t_LowerBound / t_TotalPriority;
						t_Result[pointerIndex].higherPriority = (t_LowerBound + priorityList[pointerIndex]) / t_TotalPriority;

						break;
					}
				}
			}
			return t_Result;
		}

        public string GetCurrencyInFormatInNonDecimal(double t_AmountOfCurrency) {

            string t_Result = "";
            if (t_AmountOfCurrency >= 1000000000000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000000000000)).ToString("D0") + "Qt";
            }
            else if (t_AmountOfCurrency >= 1000000000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000000000)).ToString("D0") + "Qd";
            }
            else if (t_AmountOfCurrency >= 1000000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000000)).ToString("D0") + "T";
            }
            else if (t_AmountOfCurrency >= 1000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000)).ToString("D0") + "B";
            }
            else if (t_AmountOfCurrency >= 1000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000)).ToString("D0") + "M";
            }
            else if (t_AmountOfCurrency >= 1000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000)).ToString("D0") + "K";
            }
            else if (t_AmountOfCurrency == 0)
            {
                t_Result = "0";
            }
            else
            {
                t_Result = t_AmountOfCurrency.ToString("F0");
            }

            return t_Result;
        }


        public string GetCurrencyInFormat(double t_AmountOfCurrency)
		{

			string t_Result = "";
            if (t_AmountOfCurrency >= 1000000000000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000000000000)).ToString("#.00") + "Qt";
            }
            else if (t_AmountOfCurrency >= 1000000000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000000000)).ToString("#.00") + "Qd";
            }
            else if (t_AmountOfCurrency >= 1000000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000000)).ToString("#.00") + "T";
            }
            else if (t_AmountOfCurrency >= 1000000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000000)).ToString("#.00") + "B";
            }
            else if (t_AmountOfCurrency >= 1000000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000000)).ToString("#.00") + "M";
            }
            else if (t_AmountOfCurrency >= 1000)
            {
                t_Result = Convert.ToDouble((t_AmountOfCurrency / 1000)).ToString("#.00") + "K";
            }
            else if (t_AmountOfCurrency == 0) {
                t_Result = "0";
            }
            else
            {
                t_Result = t_AmountOfCurrency.ToString("#.00");
            }

			return t_Result;
		}

		#region Translation By Camera

		private Camera m_CameraReference;
		private Vector2 m_CameraPosition;
		private float m_CameraOrthographicSize;

		private Vector2 m_ViewBoundary;

		private void PreprocessForTranslationByCamera()
		{

			m_CameraReference = Camera.main;
		}

		public bool IsHitBoundary(Vector2 t_RequestedPosition, UnitVector2D t_LowerBoundOfView, UnitVector2D t_UpperBoundOfView)
		{

			bool t_Result = false;

			m_CameraPosition = (Vector2)m_CameraReference.transform.position;
			m_CameraOrthographicSize = m_CameraReference.orthographicSize;

			if (DeviceInfoManager.Instance.IsPortraitMode())
			{

				m_ViewBoundary = new Vector2(
					m_CameraPosition.x + (m_CameraOrthographicSize / DeviceInfoManager.Instance.GetAspectRatioFactor()),
					m_CameraPosition.y + m_CameraOrthographicSize
				);
			}
			else
			{

				m_ViewBoundary = new Vector2(
					m_CameraPosition.x + (m_CameraOrthographicSize * DeviceInfoManager.Instance.GetAspectRatioFactor()),
					m_CameraPosition.y + m_CameraOrthographicSize
				);
			}

			float t_BoundaryLimitOnAxis = m_ViewBoundary.x * t_LowerBoundOfView.x;
			if (t_BoundaryLimitOnAxis > t_RequestedPosition.x)
			{
				//Negetive : X-Axis
				t_Result = true;
			}

			t_BoundaryLimitOnAxis = m_ViewBoundary.y * t_LowerBoundOfView.y;
			if (t_BoundaryLimitOnAxis > t_RequestedPosition.y)
			{
				//Negetive : Y-Axis
				t_Result = true;
			}

			t_BoundaryLimitOnAxis = m_ViewBoundary.x * t_UpperBoundOfView.x;
			if (t_RequestedPosition.x > t_BoundaryLimitOnAxis)
			{
				//Positive : X-Axis
				t_Result = true;
			}

			t_BoundaryLimitOnAxis = m_ViewBoundary.y * t_UpperBoundOfView.y;
			if (t_RequestedPosition.y > t_BoundaryLimitOnAxis)
			{
				//Positive : Y-Axis
				t_Result = true;
			}

			return t_Result;
		}

		public Vector2 TranslationByBoundedCameara(Vector2 t_RequestedPosition, UnitVector2D t_LowerBoundOfView, UnitVector2D t_UpperBoundOfView)
		{

			m_CameraPosition = (Vector2)m_CameraReference.transform.position;
			m_CameraOrthographicSize = m_CameraReference.orthographicSize;

			if (DeviceInfoManager.Instance.IsPortraitMode())
			{

				m_ViewBoundary = new Vector2(
					m_CameraPosition.x + (m_CameraOrthographicSize / DeviceInfoManager.Instance.GetAspectRatioFactor()),
					m_CameraPosition.y + m_CameraOrthographicSize
				);
			}
			else
			{

				m_ViewBoundary = new Vector2(
					m_CameraPosition.x + (m_CameraOrthographicSize * DeviceInfoManager.Instance.GetAspectRatioFactor()),
					m_CameraPosition.y + m_CameraOrthographicSize
				);
			}

			float t_BoundaryLimitOnAxis = m_ViewBoundary.x * t_LowerBoundOfView.x;
			if (t_BoundaryLimitOnAxis > t_RequestedPosition.x)
			{
				//Negetive : X-Axis
				t_RequestedPosition = new Vector2(
					t_BoundaryLimitOnAxis,
					t_RequestedPosition.y
				);
			}

			t_BoundaryLimitOnAxis = m_ViewBoundary.y * t_LowerBoundOfView.y;
			if (t_BoundaryLimitOnAxis > t_RequestedPosition.y)
			{
				//Negetive : Y-Axis
				t_RequestedPosition = new Vector2(
					t_RequestedPosition.x,
					t_BoundaryLimitOnAxis
				);
			}

			t_BoundaryLimitOnAxis = m_ViewBoundary.x * t_UpperBoundOfView.x;
			if (t_RequestedPosition.x > t_BoundaryLimitOnAxis)
			{
				//Positive : X-Axis
				t_RequestedPosition = new Vector2(
					t_BoundaryLimitOnAxis,
					t_RequestedPosition.y
				);
			}

			t_BoundaryLimitOnAxis = m_ViewBoundary.y * t_UpperBoundOfView.y;
			if (t_RequestedPosition.y > t_BoundaryLimitOnAxis)
			{
				//Positive : Y-Axis
				t_RequestedPosition = new Vector2(
					t_RequestedPosition.x,
					t_BoundaryLimitOnAxis
				);
			}

			return t_RequestedPosition;
		}

		#endregion
	}
}

